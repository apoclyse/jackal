Jackal with Genova 3 lite arm

Before you run example, make sure:
- export env JACKAL_URDF_EXTRAS=$(catkin_find jackal urdf/kortex_robot.xacro --first-only)
- You have already built the packages using `catkin_make` or `catkin build`
- You have sourced ros kortex packages https://github.com/Kinovarobotics/ros_kortex
- You have installed jackal https://www.clearpathrobotics.com/assets/guides/melodic/jackal/simulation.html

To run the example: `roslaunch jackal sim_jackal.launch`