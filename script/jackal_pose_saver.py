#!/usr/bin/env python

import rospy
from sensor_msgs.msg import JointState
from jackal.srv import DatabaseSave, DatabaseSaveRequest, DatabaseSaveResponse, DatabaseRead, DatabaseReadRequest, DatabaseReadResponse

def callback(data):
    rospy.loginfo(rospy.get_caller_id() + "I heard %s %f %f", data.position)

def main():
    rospy.init_node('jackal_pose_server')
    positions = rospy.wait_for_message("/joint_states", JointState).position
    positions = [
        positions[2],
        positions[3],
        positions[4],
        positions[5],
        positions[6],
        positions[7],
        positions[8],
    ]

    rospy.wait_for_service('database_save')

    try:
        database_saver = rospy.ServiceProxy('database_save', DatabaseSave)
        req = DatabaseSaveRequest()
        if rospy.has_param("~pose_name"):
            req.name = rospy.get_param("~pose_name")
        else:
            raise ValueError("you must specify pose_name")
        req.joint_0 = positions[0]
        req.joint_1 = positions[1]
        req.joint_2 = positions[2]
        req.joint_3 = positions[3]
        req.joint_4 = positions[4]
        req.joint_5 = positions[5]
        req.joint_gripper = positions[6]
        response = database_saver(req)
        rospy.loginfo("%s %d", rospy.get_caller_id(), response.result)
    except rospy.ServiceException as e:
        rospy.logerr("%s %s", rospy.get_caller_id(), e)
    except ValueError as e:
        rospy.logerr("%s %s", rospy.get_caller_id(), e)

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
