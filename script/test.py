#!/usr/bin/env python

import rospy
from kortex_driver.msg import Base_JointSpeeds, JointSpeed


def getJointSpeed(joint_identifier, value):
    jointSpeed = JointSpeed()
    jointSpeed.joint_identifier = joint_identifier
    jointSpeed.value = value
    jointSpeed.duration = 0
    return jointSpeed


def getRobotDescription():
    pub = rospy.Publisher('/in/joint_velocity', Base_JointSpeeds, queue_size=10)
    rospy.init_node("my_joint_speeds_publisher", anonymous=True)

    rospy.loginfo("my_joint_speeds_publisher node started...")
    rate = rospy.Rate(100)

    while not rospy.is_shutdown():
        msg = Base_JointSpeeds()
        msg.joint_speeds = [
            getJointSpeed(0, -20),
            getJointSpeed(1, 0),
            getJointSpeed(2, 0),
            getJointSpeed(3, 0),
            getJointSpeed(4, 0),
            getJointSpeed(5, 0),
        ]
        msg.duration = 0
        rospy.loginfo(msg)
        pub.publish(msg)
        rate.sleep()


if __name__ == '__main__':
    try:
        getRobotDescription()
    except rospy.ROSInterruptException:
        pass
