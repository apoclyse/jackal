#!/usr/bin/env python

import numpy as np

class Trans:
    def __init__(self):
        self.nodes = np.array([
            [1, 1, 1, 1],
            [2, 2, 2, 1],
        ])

        matrix = self.translationMatrix(-10, 5, 3.2)
        matrixR = self.rotateZMatrix(0.5)
        self.transform(matrix)
        self.transform(matrixR)

        print self.nodes

    def transform(self, matrix):
        self.nodes = np.dot(self.nodes, matrix)

    def translationMatrix(self, dx, dy, dz):
        """ Return matrix for translation along vector (dx, dy, dz). """
        return np.array([
            [1, 0, 0, 0],
            [0, 1, 0, 0],
            [0, 0, 1, 0],
            [dx, dy, dz, 1]
        ])

    def rotateXMatrix(self, radians):
        """ Return matrix for rotating about the x-axis by 'radians' radians """

        c = np.cos(radians)
        s = np.sin(radians)
        return np.array([
            [1, 0, 0, 0],
            [0, c, -s, 0],
            [0, s, c, 0],
            [0, 0, 0, 1]
        ])

    def rotateYMatrix(self, radians):
        """ Return matrix for rotating about the y-axis by 'radians' radians """

        c = np.cos(radians)
        s = np.sin(radians)
        return np.array([
            [c, 0, s, 0],
            [0, 1, 0, 0],
            [-s, 0, c, 0],
            [0, 0, 0, 1]
        ])

    def rotateZMatrix(self, radians):
        """ Return matrix for rotating about the z-axis by 'radians' radians """

        c = np.cos(radians)
        s = np.sin(radians)
        return np.array([
            [c, -s, 0, 0],
            [s, c, 0, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 1]
        ])


Trans()