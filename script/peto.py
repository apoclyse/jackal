#!/usr/bin/env python

import sys
import copy
from syslog import LOG_INFO
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_test4')

robot = moveit_commander.RobotCommander("/my_gen3/robot_description")
scene = moveit_commander.PlanningSceneInterface()

group_name = "arm"
gripper_group_name = "gripper"
move_group = moveit_commander.MoveGroupCommander(group_name)
# gripper_group = moveit_commander.MoveGroupCommander(gripper_group_name)

display_trajectory_publisher = rospy.Publisher('/my_gen3/move_group/display_planned_path',
                                                   moveit_msgs.msg.DisplayTrajectory,
                                                   queue_size=20)

###########################GRIPER CONTROL##################################################################
gripper_joint_names = rospy.get_param(rospy.get_namespace() + "gripper_joint_names", [])
gripper_joint_name = gripper_joint_names[0]

def reach_gripper_position(relative_position):
    gripper_joint = robot.get_joint(gripper_joint_name)
    gripper_joint.move(relative_position, True)
###################################################################################

joint_goal = move_group.get_current_joint_values()
joint_goal[0] = -0.8354837013863499
joint_goal[1] = -2.40998368630922
joint_goal[2] = -1.529053407236776
joint_goal[3] = -0.02407604151875198
joint_goal[4] = -0.8836736013113349
joint_goal[5] = 0.25260082939401574

# The go command can be called with joint values, poses, or without any
# parameters if you have already set the pose or joint target for the group
move_group.go(joint_goal, wait=True)

# Calling `stop()` ensures that there is no residual movement
move_group.stop()
rospy.loginfo("Reached and stop position 1")

# reach_gripper_position(0.8)

joint_goal = move_group.get_current_joint_values()
joint_goal[0] = -0.8354837013863499
joint_goal[1] = -2.40998368630922
joint_goal[2] = -0.9908269530351088
joint_goal[3] = -0.02407604151875198
joint_goal[4] = -0.8836736013113349
joint_goal[5] = 0.19851655502835955

move_group.go(joint_goal, wait=True)
move_group.stop()
rospy.loginfo("Reached and stop position 2")

# reach_gripper_position(0.01)

joint_goal = move_group.get_current_joint_values()
joint_goal[0] = -0.7932331833244612
joint_goal[1] = -1.6195332400077822
joint_goal[2] = 0.12587750471590328
joint_goal[3] = 0.09266682065817915
joint_goal[4] = -0.8827675939084898
joint_goal[5] = -0.20545914967408763

move_group.go(joint_goal, wait=True)
move_group.stop()
rospy.loginfo("Reached and stop position 3")


joint_goal = move_group.get_current_joint_values()
joint_goal[0] = -0.28287884045934497
joint_goal[1] = -1.1455869724051393
joint_goal[2] = 0.6699823543917033
joint_goal[3] = 0.09268173436028361
joint_goal[4] = -0.9007844113150929
joint_goal[5] = -0.27556686537042285

move_group.go(joint_goal, wait=True)
move_group.stop()
rospy.loginfo("Reached and stop position 4")


joint_goal = move_group.get_current_joint_values()
joint_goal[0] = -0.28287884045934497
joint_goal[1] = -1.1455869724051393
joint_goal[2] = 0.6699823543917033
joint_goal[3] = 0.09268173436028361
joint_goal[4] = -0.9007844113150929
joint_goal[5] = -1.7451545486268207

move_group.go(joint_goal, wait=True)
move_group.stop()
rospy.loginfo("Reached and stop position 5")

joint_goal = move_group.get_current_joint_values()
joint_goal[0] = -0.22094543141289869
joint_goal[1] =  -0.9492294420725065
joint_goal[2] = 0.8442348487283509
joint_goal[3] = 0.09100607340240574
joint_goal[4] = -0.8334325347147713
joint_goal[5] = -2.3461559707794004

move_group.go(joint_goal, wait=True)
move_group.stop()
rospy.loginfo("Reached and stop position 6")

joint_goal = move_group.get_current_joint_values()
joint_goal[0] = -0.22094543141289869
joint_goal[1] =  -0.9492294420725065
joint_goal[2] = 0.8442348487283509
joint_goal[3] = 0.09100607340240574
joint_goal[4] = -0.08334325347147713
joint_goal[5] = -2.3461559707794004

move_group.go(joint_goal, wait=True)
move_group.stop()
rospy.loginfo("Reached and stop position 7")

joint_goal = move_group.get_current_joint_values()
joint_goal[0] = -0.22094543141289869
joint_goal[1] = -0.9492294420725065
joint_goal[2] = 0.8442348487283509
joint_goal[3] = 0.09100607340240574
joint_goal[4] = -0.8334325347147713
joint_goal[5] = -2.3461559707794004

move_group.go(joint_goal, wait=True)
move_group.stop()
rospy.loginfo("Reapeat")

joint_goal = move_group.get_current_joint_values()
joint_goal[0] = -0.28287884045934497
joint_goal[1] = -1.1455869724051393
joint_goal[2] = 0.6699823543917033
joint_goal[3] = 0.09268173436028361
joint_goal[4] = -0.9007844113150929
joint_goal[5] = -0.27556686537042285

move_group.go(joint_goal, wait=True)
move_group.stop()
rospy.loginfo("Reached and stop position 8")

joint_goal = move_group.get_current_joint_values()
joint_goal[0] = -0.8354837013863499
joint_goal[1] = -2.40998368630922
joint_goal[2] = -0.9908269530351088
joint_goal[3] = -0.02407604151875198
joint_goal[4] = -0.8836736013113349
joint_goal[5] = 0.19851655502835955

move_group.go(joint_goal, wait=True)
move_group.stop()
rospy.loginfo("Reached and stop position 9")