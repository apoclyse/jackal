#!/usr/bin/env python

import rospy
from sensor_msgs.msg import JointState
from jackal.srv import DatabaseSave, DatabaseSaveRequest, DatabaseSaveResponse, DatabaseRead, DatabaseReadRequest, DatabaseReadResponse
from kortex_driver.msg import ActionNotification, ActionEvent, Action, JointAngle, ConstrainedJointAngles, JointAngles, JointTrajectoryConstraint
from kortex_driver.srv import ReadAction, ReadActionRequest, ExecuteAction, ExecuteActionRequest

def callback(data):
    rospy.loginfo(rospy.get_caller_id() + "I heard %s %f %f", data.position)

def getAction(pose):
    actionMsg = Action()
    actionMsg.handle.identifier = 2
    actionMsg.handle.action_type = 7
    actionMsg.handle.permission = 7
    actionMsg.name = "name"
    application_data = ""
    actionMsg.oneof_action_parameters.send_twist_command = []
    actionMsg.oneof_action_parameters.send_wrench_command = []
    actionMsg.oneof_action_parameters.send_joint_speeds = []
    actionMsg.oneof_action_parameters.reach_pose = []

    reach_joint_angles = []
    reach_joint_angle = ConstrainedJointAngles()
    reach_joint_angle.joint_angles = JointAngles()

    reach_joint_angle.constraint = JointTrajectoryConstraint()
    reach_joint_angle.constraint.type = 0
    reach_joint_angle.constraint.value = 0.0

    reach_joint_angle.joint_angles.joint_angles = []
    jointAngle = JointAngle()

    jointAngle.joint_identifier = 0
    jointAngle.value = pose.joint_0
    reach_joint_angle.joint_angles.joint_angles.append(jointAngle)

    jointAngle.joint_identifier = 1
    jointAngle.value = pose.joint_1
    reach_joint_angle.joint_angles.joint_angles.append(jointAngle)

    jointAngle.joint_identifier = 2
    jointAngle.value = pose.joint_2
    reach_joint_angle.joint_angles.joint_angles.append(jointAngle)

    jointAngle.joint_identifier = 3
    jointAngle.value = pose.joint_3
    reach_joint_angle.joint_angles.joint_angles.append(jointAngle)

    jointAngle.joint_identifier = 4
    jointAngle.value = pose.joint_4
    reach_joint_angle.joint_angles.joint_angles.append(jointAngle)

    jointAngle.joint_identifier = 5
    jointAngle.value = pose.joint_5
    reach_joint_angle.joint_angles.joint_angles.append(jointAngle)

    reach_joint_angles.append(reach_joint_angle)

    actionMsg.oneof_action_parameters.reach_joint_angles = reach_joint_angles
    actionMsg.oneof_action_parameters.toggle_admittance_mode = []
    actionMsg.oneof_action_parameters.snapshot = []
    actionMsg.oneof_action_parameters.switch_control_mapping = []
    actionMsg.oneof_action_parameters.navigate_joints = []
    actionMsg.oneof_action_parameters.navigate_mappings = []
    actionMsg.oneof_action_parameters.change_twist = []
    actionMsg.oneof_action_parameters.change_joint_speeds = []
    actionMsg.oneof_action_parameters.change_wrench = []
    actionMsg.oneof_action_parameters.apply_emergency_stop = []
    actionMsg.oneof_action_parameters.clear_faults = []
    actionMsg.oneof_action_parameters.delay = []
    actionMsg.oneof_action_parameters.execute_action = []
    actionMsg.oneof_action_parameters.send_gripper_command = []
    actionMsg.oneof_action_parameters.send_gpio_command = []
    actionMsg.oneof_action_parameters.stop_action = []
    actionMsg.oneof_action_parameters.play_pre_computed_trajectory = []
    actionMsg.oneof_action_parameters.execute_sequence = []
    actionMsg.oneof_action_parameters.execute_waypoint_list = []

    return actionMsg

def main():
    rospy.init_node('jackal_sequencer')
    rospy.wait_for_service('database_read')

    # # Init the services
    # read_action_full_name = '/base/read_action'
    # rospy.wait_for_service(read_action_full_name)
    # read_action = rospy.ServiceProxy(read_action_full_name, ReadAction)
    # req = ReadActionRequest()
    # req.input.identifier = 2
    #
    # try:
    #     res = read_action(req)
    #     print(res)
    # except rospy.ServiceException:
    #     rospy.logerr("Failed to call ReadAction")

    try:
        if not rospy.has_param("~sequence"):
            raise ValueError("you must specify sequence")

        database_reader = rospy.ServiceProxy('database_read', DatabaseRead)
        req = DatabaseReadRequest()

        sequence = rospy.get_param("~sequence")
        poses = []
        for s in sequence:
            req.name = s['name']
            poses.append(getAction(database_reader(req)))

        print(poses)
    except rospy.ServiceException as e:
        rospy.logerr("%s %s", rospy.get_caller_id(), e)
    except ValueError as e:
        rospy.logerr("%s %s", rospy.get_caller_id(), e)

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
