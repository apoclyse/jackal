#!/usr/bin/env python

import rospy
from std_msgs.msg import Empty
import threading
from flask import Flask, request
from kortex_driver.msg import Base_JointSpeeds, JointSpeed
app = Flask(__name__)

baseJointSpeeds = None

def getJointSpeed(joint_identifier, value):
    jointSpeed = JointSpeed()
    jointSpeed.joint_identifier = joint_identifier
    jointSpeed.value = value
    jointSpeed.duration = 0
    return jointSpeed


def getBaseJointSpeeds(actions):
    print(actions)

    msg = Base_JointSpeeds()
    msg.joint_speeds = []
    msg.duration = 0


    for a in actions:
        msg.joint_speeds.append(getJointSpeed(int(a['name']), a['speed']))

    return msg

def publishJointSpeeds():
    pub = rospy.Publisher('/in/joint_velocity', Base_JointSpeeds, queue_size=10)

    while not rospy.is_shutdown():
        if not baseJointSpeeds is None:
            rate = rospy.Rate(100)
            pub.publish(baseJointSpeeds)
            rospy.loginfo(baseJointSpeeds)
        else:
            rate = rospy.Rate(1)
        rospy.loginfo(baseJointSpeeds)
        rospy.loginfo("waiting...")
        rate.sleep()


@app.route('/api/actions', methods = ['POST'])
def actions():
    data = request.get_json()
    global baseJointSpeeds
    baseJointSpeeds = getBaseJointSpeeds(data["actions"])
    return 'ok'

@app.route('/api/stop', methods = ['GET'])
def stopAll():
    global baseJointSpeeds
    baseJointSpeeds = None

    pub = rospy.Publisher('/in/stop', Empty, queue_size=10)
    pub.publish(Empty())
    rospy.loginfo("stop all movements...")

    return 'ok'

@app.route('/api/emergency-stop', methods = ['GET'])
def emergencyStop():
    global baseJointSpeeds
    baseJointSpeeds = None

    pub = rospy.Publisher('/in/emergency_stop', Empty, queue_size=10)
    pub.publish(Empty())
    rospy.logwarn("emergency stop all movements...")

    return 'ok'

def startNode():
    rospy.init_node('jackal_flask', disable_signals=True)
    rospy.loginfo("jackal_flask node started...")
    publishJointSpeeds()

def main():
    thread = threading.Thread(target=startNode)
    thread.daemon = True
    thread.start()
    app.run()

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass