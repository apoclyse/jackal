#!/usr/bin/env python

import rospy
from std_msgs.msg import UInt16

def publishJointSpeeds():
    pub = rospy.Publisher('set_relay', UInt16, queue_size=10)
    rate = rospy.Rate(1)
    msg = UInt16()
    msg.data = 0

    while not rospy.is_shutdown():
        if msg.data is 0:
            msg.data = 1
        else:
            msg.data = 0

        # msg.data = 0
        pub.publish(msg)
        rospy.loginfo("publishing")
        rate.sleep()


def main():
    rospy.init_node('postrek_node')
    publishJointSpeeds()


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass


# roscore
# sudo chmod a+rw /dev/ttyUSB0
# rosrun rosserial_python serial_node.py /dev/ttyUSB0
# rostopic pub set_relay std_msgs/UInt16 "data: 1"

# scp toggle.py administrator@192.168.131.1:~/switch_ws/src/switch/src