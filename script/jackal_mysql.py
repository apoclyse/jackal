#!/usr/bin/env python

import rospy
import mysql.connector.pooling
from jackal.srv import DatabaseSave, DatabaseSaveRequest, DatabaseRead, DatabaseReadRequest, DatabaseReadResponse

db = {
    "host": "localhost",
    "database": "jackal",
    "user": "apoclyse",
    "password": "apoclyse123.A"
}


class Database:

    def __init__(self):
        try:
            self.db_pool = mysql.connector.pooling.MySQLConnectionPool(pool_name="mypool", pool_size=3, **db)
            print("connected to database")
        except Exception as e:
            print(e)

        # init servers
        self.databaseSaver = rospy.Service('database_save', DatabaseSave, self.save)
        self.databaseReader = rospy.Service('database_read', DatabaseRead, self.read)

    def save(self, req):
        connection = self.db_pool.get_connection()
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.execute(
                "INSERT INTO poses (created, name , joint_0, joint_1, joint_2, joint_3, joint_4, joint_5, joint_gripper) values (now(), %s, %s, %s, %s, %s, %s, %s, %s);",
                (req.name, req.joint_0, req.joint_1, req.joint_2, req.joint_3, req.joint_4, req.joint_5,
                 req.joint_gripper))
            connection.commit()
            cursor.close()
            connection.close()
        print(req)
        return 1

    def read(self, req):
        connection = self.db_pool.get_connection()
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.execute(
                "SELECT name, joint_0, joint_1, joint_2, joint_3, joint_4, joint_5, joint_gripper FROM poses WHERE name=%s;",
                (req.name,)
            )
            pose = cursor.fetchone()
            cursor.close()
            connection.close()
        return pose


def main():
    rospy.init_node('jackal_mysql')
    database = Database()
    rospy.spin()


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
