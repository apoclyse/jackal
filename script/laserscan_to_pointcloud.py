#!/usr/bin/env python

import math

import message_filters
import rospy
import numpy as np
import ros_numpy
import tf.listener
from sensor_msgs import point_cloud2
from sensor_msgs.msg import PointCloud2, LaserScan, PointField
from std_msgs.msg import Header
import laser_geometry.laser_geometry as lg

class MatrixHelper:
    def __init__(self, dx, dy, dz, roll, pitch, yaw):
        translationMatrix = self.getTranslationMatrix(dx, dy, dz)
        rotationXMatrix = self.getRotationXMatrix(-roll)
        rotationYMatrix = self.getRotationYMatrix(-pitch)
        rotationZMatrix = self.getRotationZMatrix(-yaw)

        transformMatrix = np.dot(rotationXMatrix, rotationYMatrix)
        transformMatrix = np.dot(transformMatrix, rotationZMatrix)
        self.transformMatrix = np.dot(transformMatrix, translationMatrix)

    def getTranslationMatrix(self, dx, dy, dz):
        """ Return matrix for translation along vector (dx, dy, dz). """
        return np.array([
            [1, 0, 0, 0],
            [0, 1, 0, 0],
            [0, 0, 1, 0],
            [dx, dy, dz, 1]
        ])

    def getRotationXMatrix(self, radians):
        """ Return matrix for rotating about the x-axis by 'radians' radians """

        c = np.cos(radians)
        s = np.sin(radians)
        return np.array([
            [1, 0, 0, 0],
            [0, c, -s, 0],
            [0, s, c, 0],
            [0, 0, 0, 1]
        ])

    def getRotationYMatrix(self, radians):
        """ Return matrix for rotating about the y-axis by 'radians' radians """

        c = np.cos(radians)
        s = np.sin(radians)
        return np.array([
            [c, 0, s, 0],
            [0, 1, 0, 0],
            [-s, 0, c, 0],
            [0, 0, 0, 1]
        ])

    def getRotationZMatrix(self, radians):
        """ Return matrix for rotating about the z-axis by 'radians' radians """

        c = np.cos(radians)
        s = np.sin(radians)
        return np.array([
            [c, -s, 0, 0],
            [s, c, 0, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 1]
        ])


class Location:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

class Rotation:
    def __init__(self, roll, pitch, yaw):
        self.roll = -roll
        self.pitch = -pitch
        self.yaw = yaw

class Transform:
    def __init__(self, location, rotation):
        self.location = location
        self.rotation = rotation

class LidarTf:
    def __init__(self, base_frame, front_frame, rear_frame, velodyne_frame):
        listener = tf.TransformListener()
        listener.waitForTransform(base_frame, front_frame, rospy.Time.now(), rospy.Duration(1))
        listener.waitForTransform(base_frame, rear_frame, rospy.Time.now(), rospy.Duration(1))
        listener.waitForTransform(base_frame, velodyne_frame, rospy.Time.now(), rospy.Duration(1))

        frontTf = listener.lookupTransform(base_frame, front_frame, rospy.Time.now())
        rearTf = listener.lookupTransform(base_frame, rear_frame, rospy.Time.now())
        velodyneTf = listener.lookupTransform(base_frame, velodyne_frame, rospy.Time.now())

        self.front_tf = self.getTransform(frontTf)
        self.rear_tf = self.getTransform(rearTf)
        self.velodyne_tf = self.getTransform(velodyneTf)

    def eulerFromQuaternion(self, x, y, z, w):
        """
        Convert a quaternion into euler angles (roll, pitch, yaw)
        roll is rotation around x in radians (counterclockwise)
        pitch is rotation around y in radians (counterclockwise)
        yaw is rotation around z in radians (counterclockwise)
        """
        t0 = +2.0 * (w * x + y * z)
        t1 = +1.0 - 2.0 * (x * x + y * y)
        roll_x = math.atan2(t0, t1)

        t2 = +2.0 * (w * y - z * x)
        t2 = +1.0 if t2 > +1.0 else t2
        t2 = -1.0 if t2 < -1.0 else t2
        pitch_y = math.asin(t2)

        t3 = +2.0 * (w * z + x * y)
        t4 = +1.0 - 2.0 * (y * y + z * z)
        yaw_z = math.atan2(t3, t4)

        return roll_x, pitch_y, yaw_z  # in radians

    def getTransform(self, t):
        euler = self.eulerFromQuaternion(t[1][0], t[1][1], t[1][2], t[1][3])
        return MatrixHelper(t[0][0], t[0][1], t[0][2], euler[0], euler[1], euler[2]).transformMatrix


class LidarSubscriber:
    def __init__(self, baseFrame, frontTf, rearTf, velodyneTf, frontScan, rearScan, velodyneScan):
        self.frontTf = frontTf
        self.rearTf = rearTf
        self.velodyneTf = velodyneTf

        # header sa pre point cloud 2
        self.header = Header()
        self.header.frame_id = baseFrame
        self.header.stamp = rospy.Time.now()

        # fields pre point cloud 2
        self.fields = [
            PointField('x', 0, PointField.FLOAT32, 1),
            PointField('y', 4, PointField.FLOAT32, 1),
            PointField('z', 8, PointField.FLOAT32, 1),
        ]

        self.lp = lg.LaserProjection()
        self.lidarPub = rospy.Publisher("merged_points", PointCloud2, queue_size=1)

        ts = message_filters.ApproximateTimeSynchronizer(
            [
                message_filters.Subscriber(frontScan, LaserScan),
                message_filters.Subscriber(rearScan, LaserScan),
                message_filters.Subscriber(velodyneScan, PointCloud2)
            ],
            1000,
            0.3)
        ts.registerCallback(self.mergeScans)

    def getTransformed(self, pc2, transformMatrix):
        xyzArray = ros_numpy.point_cloud2.pointcloud2_to_xyz_array(pc2)
        xyzArray = np.c_[xyzArray, np.ones(len(xyzArray))]
        points = np.dot(xyzArray, transformMatrix)
        points = np.delete(points, -1, axis=1)
        return point_cloud2.create_cloud(self.header, self.fields, points)

    def mergeScans(self, frontScan, rearScan, velodynePointCloud2):
        frontScan = self.lp.projectLaser(frontScan)
        rearScan = self.lp.projectLaser(rearScan)

        self.header.stamp = rospy.Time.now()

        frontCloud = self.getTransformed(frontScan, self.frontTf)
        rearCloud = self.getTransformed(rearScan, self.rearTf)
        velodyneCloud = self.getTransformed(velodynePointCloud2, self.velodyneTf)

        mergedCloud = PointCloud2()
        mergedCloud.header = self.header

        mergedCloud.height = frontCloud.height
        mergedCloud.width = frontCloud.width + rearCloud.width + velodyneCloud.width
        mergedCloud.fields = frontCloud.fields
        mergedCloud.is_bigendian = frontCloud.is_bigendian
        mergedCloud.point_step = frontCloud.point_step
        mergedCloud.row_step = frontCloud.row_step + rearCloud.row_step + velodyneCloud.row_step
        mergedCloud.data = frontCloud.data + rearCloud.data + velodyneCloud.data
        mergedCloud.is_dense = frontCloud.is_dense

        self.lidarPub.publish(mergedCloud)


def main():
    rospy.init_node("laserscan_to_pointcloud")
    # lidarTf = LidarTf("/map", "/laser_frame_1", "/laser_frame_2", "/point_cloud_test")
    lidarTf = LidarTf("base_link", "front_laser", "rear_laser", "velodyne")

    # lidarSub = LidarSubscriber("/map", lidarTf.front_tf, lidarTf.rear_tf, lidarTf.velodyne_tf, "/scan_test_1",
    #                            "/scan_test_2", "/point_cloud2_test")
    lidarSub = LidarSubscriber("base_link", lidarTf.front_tf, lidarTf.rear_tf, lidarTf.velodyne_tf, "/front/scan", "/rear/scan", "/velodyne_points")
    rospy.spin()


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
